package pl.wat;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    private static Stage stage;
    @Override
    public void start(Stage primaryStage) throws Exception {
        stage = primaryStage;
        Parent root = FXMLLoader.load(getClass().getResource("/MainScreen.fxml"));
        primaryStage.setTitle("Silnik jezyka skryptowego");
        primaryStage.setResizable(false);
        primaryStage.setScene(new Scene(root, 900, 400));
        primaryStage.show();
    }

    public static void main(String[] args){
        launch(args);
    }

    public static Stage getStage() {
        return stage;
    }
}
