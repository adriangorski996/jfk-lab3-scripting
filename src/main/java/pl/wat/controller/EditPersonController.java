package pl.wat.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import pl.wat.model.Person;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.function.UnaryOperator;
import java.util.regex.Pattern;

public class EditPersonController implements Initializable {

    @FXML
    TextField imieField;
    @FXML
    TextField nazwiskoField;
    @FXML
    TextField wiekField;
    @FXML
    TextField pensjaField;
    @FXML
    TextField mailField;

    public ButtonType cancelButton;
    public ButtonType applyButton;
    public DialogPane editDialogPane;
    Person person;
    private Pattern pattern = Pattern.compile("\\d*|\\d+\\.\\d*");
    private TextFormatter formatter = new TextFormatter((UnaryOperator<TextFormatter.Change>) change -> pattern.matcher(change.getControlNewText()).matches() ? change : null);

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        pensjaField.setTextFormatter(formatter);
        wiekField.setOnKeyReleased(e -> handle(wiekField));
        person = MainScreenController.getInstance().getPersonToEdit();

        imieField.setText(person.getFirstName());
        nazwiskoField.setText(person.getLastName());
        wiekField.setText(Integer.toString(person.getAge()));
        pensjaField.setText(Double.toString(person.getSalary()));
        mailField.setText(person.getEmail());

        ((ButtonBase) editDialogPane.lookupButton(applyButton)).setOnAction(e -> {
            if (imieField.getText().isEmpty() || nazwiskoField.getText().isEmpty() || wiekField.getText().isEmpty() || pensjaField.getText().isEmpty()) {
                AlertController.makeAlert(Alert.AlertType.WARNING, "Blad", "Nie wprowadzono wymaganych danych", "Nie można dodać osoby, ponieważ nie wprowadzono wymaganych danych");
            } else {
                person.setFirstName(imieField.getText());
                person.setLastName(nazwiskoField.getText());
                person.setId(person.getId());
                person.setAge(Integer.valueOf(wiekField.getText()));
                person.setSalary(Double.valueOf(pensjaField.getText()));
                person.setEmail(mailField.getText());

                MainScreenController.getInstance().refreshList();
                editAlert(person);
            }
        });
    }

    private void handle(TextField field) {
        if (!field.getCharacters().chars().allMatch(Character::isDigit)) {
            AlertController.makeAlert(Alert.AlertType.WARNING, "Blad", "Zly format danych", "Wprowadzono niepoprawny argument, proszę wprowadzić liczbę.");
            field.clear();
        }
    }

    private void editAlert(Person person) {
        AlertController.makeAlert(Alert.AlertType.INFORMATION, "Edycja", "Zapisan zedytowane dane", "Wprowadzono zmiany dla osoby: " + person.getFirstName() + " " + person.getLastName());
    }
}
