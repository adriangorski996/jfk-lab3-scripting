package pl.wat.controller;

import javafx.fxml.Initializable;
import javafx.scene.control.*;
import pl.wat.model.Person;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.function.UnaryOperator;
import java.util.regex.Pattern;

public class AddNewPersonController implements Initializable {

    public TextField imieField;
    public TextField nazwiskoField;
    public TextField wiekField;
    public TextField pensjaField;
    public TextField mailField;
    public ButtonType cancelButton;
    public ButtonType applyButton;
    public DialogPane addDialogPane;
    private Pattern pattern = Pattern.compile("\\d*|\\d+\\.\\d*");
    private TextFormatter formatter = new TextFormatter((UnaryOperator<TextFormatter.Change>) change -> pattern.matcher(change.getControlNewText()).matches() ? change : null);

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        wiekField.setOnKeyReleased(e -> handle(wiekField));
        pensjaField.setTextFormatter(formatter);

        ((ButtonBase) addDialogPane.lookupButton(applyButton)).setOnAction(e -> {
            if (imieField.getText().isEmpty() || nazwiskoField.getText().isEmpty() || wiekField.getText().isEmpty() || pensjaField.getText().isEmpty()) {
                AlertController.makeAlert(Alert.AlertType.WARNING, "Blad", "Nie wprowadzono wymaganych danych", "Nie można dodać osoby, ponieważ nie wprowadzono wymaganych danych");
            } else {
                Person person = new Person();
                person.setFirstName(imieField.getText());
                person.setLastName(nazwiskoField.getText());
                person.setId(MainScreenController.getInstance().getPersonList().iterator().hasNext() ? MainScreenController.getInstance().getPersonList().iterator().next().getId() + 1 : 1);
                person.setAge(Integer.valueOf(wiekField.getText()));
                person.setSalary(Double.valueOf(pensjaField.getText()));
                person.setEmail(mailField.getText());

                MainScreenController.getInstance().addPerson(person);
                System.out.println(person.getId());
            }
        });
    }

    private void handle(TextField field) {
        if (!field.getCharacters().chars().allMatch(Character::isDigit)) {
            AlertController.makeAlert(Alert.AlertType.WARNING, "Blad", "Zly format danych", "Wprowadzono niepoprawny argument, proszę wprowadzić liczbę.");
            field.clear();
        }
    }

}
