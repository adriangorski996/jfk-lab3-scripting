package pl.wat.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import org.renjin.script.RenjinScriptEngine;
import org.renjin.script.RenjinScriptEngineFactory;
import pl.wat.model.Person;

import javax.script.ScriptEngine;
import java.io.StringWriter;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class RController implements Initializable {

    @FXML
    Button runRScriptButton;
    @FXML
    TextArea rScriptTextArea;
    @FXML
    TextArea rOutputTextArea;
    private ScriptEngine engine;
    private StringWriter writer;
    private static RController instance = null;
    private List<Person> personList;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        instance = this;
        rOutputTextArea.setEditable(false);
        runRScriptButton.setDisable(true);
        RenjinScriptEngineFactory factory = new RenjinScriptEngineFactory();
        this.engine = factory.getScriptEngine();
        this.writer = new StringWriter();
        engine.getContext().setWriter(writer);
        engine.getContext().setErrorWriter(writer);
        runRScriptButton.setOnAction(e -> rOutputTextArea.setText(runScript(rScriptTextArea.getText())));
    }

    static RController getInstance() {
        return instance;
    }

    void setPersonList(List<Person> personList) {
        runRScriptButton.setDisable(false);
        this.personList = personList;
    }


    private String runScript(String in) {
        writer.getBuffer().setLength(0);
        engine.put("people", this.personList);
        try {
            engine.eval(in);
        } catch (Exception e) {
            ((RenjinScriptEngine) engine).getSession().getConnectionTable().getStderr().getStream().append(e.getMessage());
        } finally {
            MainScreenController.getInstance().refreshList();
        }
        return writer.toString();

    }

}
