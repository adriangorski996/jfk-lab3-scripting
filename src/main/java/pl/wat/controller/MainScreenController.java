package pl.wat.controller;

import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import pl.wat.model.Person;

import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

public class MainScreenController implements Initializable {
    public BorderPane mainBorderPane;
    public AnchorPane mainPane;
    public AnchorPane pythonPane;
    public AnchorPane rPane;
    public Tab pythonTab;
    public Tab rTab;
    public TableView<Person> dataTableView;
    public TableColumn<Person, String> nameTableColumn;
    public TableColumn<Person, String> secondNameTableColumn;
    public TableColumn<Person, Integer> ageTableColumn;
    public TableColumn<Person, String> numberTableColumn;
    public TableColumn<Person, String> mailTableColumn;
    public Button removeElementButton;
    public Button addElementButton;
    public Button editElementButton;
    private static MainScreenController instance;

    private List<Person> personList;
    private ObservableList<Person> personObservableList = FXCollections.observableArrayList();
    private Person personToEdit;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        removeElementButton.setDisable(true);
        addElementButton.setDisable(true);
        editElementButton.setDisable(true);
        instance = this;
        setColumnFactories();
        removeElementButton.setOnAction(e -> removePersonFromList());
        addElementButton.setOnAction(e -> showDialogToAddNewPerson());
        editElementButton.setOnAction(e -> showDialogToEditPerson());
        dataTableView.setItems(personObservableList);

    }

    private void removePersonFromList() {
        Person personToRemove = dataTableView.getSelectionModel().getSelectedItem();
        if (personToRemove == null) {
            AlertController.makeAlert(Alert.AlertType.WARNING, "Usuwanie", "Nie usunieto", "Prosze wybrac osobe do usuniecia");
            return;
        }
        personList.remove(personToRemove);
        personObservableList.setAll(personList);
        AlertController.makeAlert(Alert.AlertType.INFORMATION, "Usuwanie", "Usunieto osobe", "Usunieto osobe: " + personToRemove.getFirstName() + " " + personToRemove.getLastName() + " " + personToRemove.getEmail());
    }


    void setPersonList(List<Person> personList) {
        removeElementButton.setDisable(false);
        addElementButton.setDisable(false);
        editElementButton.setDisable(false);
        this.personList = personList;
        refreshList();
    }

    void refreshList() {
        this.personObservableList.clear();
        this.personObservableList.setAll(this.personList);
    }

    private void setColumnFactories() {
        nameTableColumn.setCellValueFactory(element -> new SimpleStringProperty(element.getValue().getFirstName()));
        secondNameTableColumn.setCellValueFactory(element -> new SimpleStringProperty(element.getValue().getLastName()));
        ageTableColumn.setCellValueFactory(element -> new SimpleIntegerProperty(element.getValue().getAge()).asObject());
        numberTableColumn.setCellValueFactory(element -> Bindings.format("%.2f", element.getValue().getSalary()));
        mailTableColumn.setCellValueFactory(element -> new SimpleStringProperty(element.getValue().getEmail()));
    }

    static MainScreenController getInstance() {
        return instance;
    }

    private void showDialogToAddNewPerson() {
        DialogPane dialogPane = null;
        Dialog dialog = new Dialog();
        try {
            dialogPane = FXMLLoader.load(getClass().getResource("/AddDialogPane.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        dialog.setDialogPane(dialogPane);
        dialog.show();
    }

    private void showDialogToEditPerson() {
        personToEdit = dataTableView.getSelectionModel().getSelectedItem();
        if (personToEdit == null) {
            AlertController.makeAlert(Alert.AlertType.WARNING, "Blad edycji", "Brak zaznaczonej osoby do edycji", "Wybierz osobe do edycji");
            return;
        }
        DialogPane dialogPane = null;
        Dialog dialog = new Dialog();
        try {
            dialogPane = FXMLLoader.load(getClass().getResource("/EditDialogPane.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        dialog.setDialogPane(dialogPane);
        dialog.show();
    }

    void addPerson(Person person) {
        this.personList.add(person);
        this.personObservableList.add(person);
        AlertController.makeAlert(Alert.AlertType.INFORMATION, "Zapis", "Zapisan osobe", "Wprowadzono osobe: " + person.getFirstName() + " " + person.getLastName());
    }

    List<Person> getPersonList() {
        return Collections.unmodifiableList(personList);
    }

    public Person getPersonToEdit() {
        return personToEdit;
    }
}
