package pl.wat.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import org.python.core.Options;
import org.python.util.InteractiveInterpreter;
import pl.wat.model.Person;

import javax.script.*;
import java.io.StringWriter;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class PythonController implements Initializable {
    @FXML
    private TextArea pythonScriptTextArea;
    @FXML
    private TextArea pythonOutputTextArea;
    @FXML
    private Button runPythonScriptButton;


    private ScriptEngine engine;
    private ScriptContext context = new SimpleScriptContext();
    private StringWriter writer = new StringWriter();
    private static PythonController instance;
    private List<Person> personList;
    private InteractiveInterpreter interpreter;


    void setPersonList(List<Person> personList) {
        runPythonScriptButton.setDisable(false);
        this.personList = personList;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        runPythonScriptButton.setDisable(true);
        pythonOutputTextArea.setEditable(false);
        instance = this;
        Options.importSite = false;

        interpreter = new InteractiveInterpreter();
        engine = new ScriptEngineManager().getEngineByName("python");
        engine.setContext(context);
        engine.getContext().setWriter(writer);
        engine.getContext().setErrorWriter(writer);
        runPythonScriptButton.setOnAction(e -> {
            pythonOutputTextArea.setText(runScript2(pythonScriptTextArea.getText()));
        });
    }

    private String runScript(String in) {
        writer.getBuffer().setLength(0);
        engine.put("people", personList);

        try {
            engine.eval(in, context);
            MainScreenController.getInstance().refreshList();
        } catch (ScriptException e) {
            e.printStackTrace();
        }

        return writer.toString();
    }

    private String runScript2(String in) {
        writer.getBuffer().setLength(0);

        interpreter.setOut(writer);
        interpreter.setErr(writer);
        interpreter.set("people", personList);

        try {
            interpreter.exec(in);
        } catch (Exception e) {
            e.printStackTrace();
        }

        MainScreenController.getInstance().refreshList();
        interpreter.close();
        return writer.toString();
    }


    static PythonController getInstance() {
        return instance;
    }
}
