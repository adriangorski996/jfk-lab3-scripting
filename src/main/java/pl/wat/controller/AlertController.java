package pl.wat.controller;

import javafx.scene.control.Alert;

public class AlertController {
    public static void makeAlert(Alert.AlertType type, String title, String header, String context) {
        Alert alert = new Alert(type, context);
        alert.setHeaderText(header);
        alert.setTitle(title);
        alert.showAndWait();
    }
}
