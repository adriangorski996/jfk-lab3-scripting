package pl.wat.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.stage.FileChooser;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import pl.wat.Main;
import pl.wat.model.Person;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

public class DataImportController implements Initializable {

    @FXML
    private Button loadFileButton;
    @FXML
    private Button saveFileButton;
    private File file;

    private List<Person> personList;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        saveFileButton.setDisable(true);
        loadFileButton.setOnAction(e -> {
            FileChooser fileChooser = new FileChooser();
            file = fileChooser.showOpenDialog(Main.getStage());
            CSVFormat format = CSVFormat.RFC4180.withHeader().withDelimiter(';');

            CSVParser parser = null;
            try {
                parser = new CSVParser(new FileReader(file), format);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            personList = new ArrayList<>();
            try {
                parser.getRecords().forEach(r -> {
                    Person p = new Person();
                    p.setId(Integer.parseInt(r.get("ID") != null ? r.get("ID") : "-100"));
                    p.setAge(Integer.parseInt(r.get("Age") != null ? r.get("Age") : "-100"));
                    p.setSalary(Double.parseDouble(r.get("Salary") != null ? r.get("Salary") : "-100"));
                    p.setEmail(r.get("Email") != null ? r.get("Email") : "");
                    p.setFirstName(r.get("FirstName") != null ? r.get("FirstName") : "");
                    p.setLastName(r.get("LastName") != null ? r.get("LastName") : "");
                    this.personList.add(p);
                });
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            this.load();
            System.out.println(file.getAbsolutePath());
            System.out.println(Arrays.toString(personList.toArray()));

        });
        saveFileButton.setOnAction(e -> {
            CSVFormat format = CSVFormat.RFC4180.withDelimiter(';');
            CSVPrinter printer = null;
            try {
                printer = new CSVPrinter(new FileWriter(file), format);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            try {
                printer.printRecord("ID", "FirstName", "LastName", "Age", "Salary", "Email");
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            for (Person p : personList) {
                List<String> empData = new ArrayList<String>();
                empData.add(Integer.toString(p.getId()));
                empData.add(p.getFirstName());
                empData.add(p.getLastName());
                empData.add(Integer.toString(p.getAge()));
                empData.add(Double.toString(p.getSalary()));
                empData.add(p.getEmail());
                try {
                    printer.printRecord(empData);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            try {
                printer.close();
                AlertController.makeAlert(Alert.AlertType.INFORMATION, "Zapis CSV", "Zapisano", "Zapisano zedytowany plik csv: " + file.getAbsolutePath());
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });
    }

    private void load() {
        saveFileButton.setDisable(false);
        MainScreenController.getInstance().setPersonList(personList);
        PythonController.getInstance().setPersonList(personList);
        RController.getInstance().setPersonList(personList);
    }
}
